/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.display;

import java.awt.Canvas;
import java.awt.Graphics2D;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

public class CanvasGraphicsManager extends GraphicsManager implements ComponentListener {

	final Canvas canvas;

	int width;
	int height;
	
	boolean ready;
	
	BufferedImage image;
	
	public CanvasGraphicsManager(Canvas canvas) {
		if (canvas == null) {
			throw new NullPointerException("Component Cannot be null!");
		}
		this.canvas = canvas;
		width = 0;
		height = 0;
		
		ready = false;
		image = null;
	}
	
	public void init() {
		System.out.println("Init of Canvas!");
		//Thread.dumpStack();
		this.canvas.setIgnoreRepaint(true);
		this.canvas.createBufferStrategy(3);
	}
	
	protected void resize(int width, int height) {
		if(width == 0 || height == 0) {
			throw new GraphicsException();
		}
		this.width = width;
		this.height = height;
	}
	
	
	/* (non-Javadoc)
	 * @see cc.plural.display.GraphicsManager#getGraphicsWidth()
	 */
	@Override
	public int getGraphicsWidth() {
		return width;
	}

	/* (non-Javadoc)
	 * @see cc.plural.display.GraphicsManager#getGraphicsHeight()
	 */
	@Override
	public int getGraphicsHeight() {
		return height;
	}

	public void requestFocus() {
		canvas.setFocusable(true);
		canvas.requestFocus();
	}
	
	public boolean ready() {
		return ready;
	}

	public Graphics getDrawGraphics() {
		Graphics2D graphics2D = (Graphics2D) canvas.getBufferStrategy().getDrawGraphics();
		graphics.manager = this;
		graphics.g = graphics2D;
		return graphics;
	}
	
	public void showGraphics() {
		BufferStrategy bufferStrategy = canvas.getBufferStrategy();
		bufferStrategy.show();
	}

	public void componentResized(ComponentEvent e) {
		resize(canvas.getWidth(), canvas.getHeight());
	}

	public void componentMoved(ComponentEvent e) {
	}

	public void componentShown(ComponentEvent e) {
	}

	public void componentHidden(ComponentEvent e) {
	}

}
