package cc.plural.display;

import cc.plural.display.GraphicsManager;
import cc.plural.state.State;
import cc.plural.timing.ScheduledActionExecuteException;
import cc.plural.timing.TimedAction;

public class DisplayAction extends TimedAction {

	State state;
	boolean bufferConfigured;
	
	public DisplayAction(State state) {
		this.state = state;
		this.bufferConfigured = false;
	}
	
	protected void handleExecute(long time, long lag) throws ScheduledActionExecuteException {
		if(!state.getStateCheck().valid()) {
			return;
		}
		if(!bufferConfigured) {
			state.getDisplay().getGraphicsManager().init();
			bufferConfigured = true;
		}
		GraphicsManager manager = state.getDisplay().getGraphicsManager();
		
		manager.showGraphics();
	}
}
