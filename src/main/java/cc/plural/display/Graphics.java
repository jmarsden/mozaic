/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.display;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.ImageObserver;

public class Graphics {
	
	java.awt.Graphics2D g;
	GraphicsManager manager;
	
	Graphics() {
		g = null;
		manager = null;
	}
	
	Graphics(java.awt.Graphics2D g) {
		this.g = g;
	}
	
	public java.awt.Graphics2D getInnerGraphics() {
		return g;
	}
	
	public void showGraphics() {
		g.dispose();
		manager.showGraphics();
	}
	
	public void setColor(Color c) {
		g.setColor(c);
	}
	
	public void drawString(String string, int x, int y) {
		g.drawString(string, x, y);
	}
	
	public void drawRectangle(int x, int y, int width, int height) {
		g.drawRect(x, y, width, height);
	}

	public void fillRect(int x, int y, int width, int height) {
		g.fillRect(x, y, width, height);
	}
	
	public void fillOval(int x, int y, int width, int height) {
		g.fillOval(x, y, width, height);
	}
	
	public void drawImage(Image img, int x, int y, ImageObserver observer) {
		g.drawImage(img, x, y, observer);
	}
	

	// drawImage
	
	// drawString
	
	
	
}
