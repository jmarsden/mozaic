/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.display;

import java.util.ArrayList;
import java.util.List;

import cc.plural.state.StateCheckCase;

public class Display implements StateCheckCase {
	
	final List<GraphicsManager> displayGraphicsManagers;

	int displayCount;

	int defaultDisplay;
	
	public Display() {
		displayGraphicsManagers = new ArrayList<GraphicsManager>();
		displayCount = 0;
		defaultDisplay = 0;
	}
	
	public void reset() {
		displayCount = 0;
		defaultDisplay = 0;
	}
	
	public boolean hasDisplay() {
		return displayCount != 0;
	}

	public int getDisplayCount() {
		synchronized (displayGraphicsManagers) {
			return displayGraphicsManagers.size();
		}
	}

	public int registerGraphicsManager(GraphicsManager manager) throws GraphicsException {
		synchronized(displayGraphicsManagers) {
			if(displayGraphicsManagers.contains(manager)) {
				displayGraphicsManagers.set(displayCount, manager);
			} else {
				displayGraphicsManagers.add(displayCount, manager);
			}
			int displayNumber = displayCount;
			displayCount++;
			return displayNumber;
		}
	}
	
	public int registerGraphicsManager(GraphicsManager manager, boolean defaultManager) throws GraphicsException {
		synchronized(displayGraphicsManagers) {
			if(displayGraphicsManagers.contains(manager)) {
				displayGraphicsManagers.set(displayCount, manager);
			} else {
				displayGraphicsManagers.add(displayCount, manager);
			}
			int displayNumber = displayCount;
			defaultDisplay = displayNumber;
			displayCount++;
			return displayNumber;
		}
	}
	
	public GraphicsManager removeGraphicsManager(GraphicsManager manager) throws GraphicsException {
		GraphicsManager graphicsManager = null;
		synchronized(displayGraphicsManagers) {
			if(!displayGraphicsManagers.contains(manager)) {
				throw new GraphicsException();
			}
			for(int i=0;i<displayGraphicsManagers.size();i++) {
				graphicsManager = displayGraphicsManagers.get(i);
				if(graphicsManager != null && graphicsManager.equals(manager)) {
					displayGraphicsManagers.set(i, null);
					return graphicsManager;
				}
			}
			return graphicsManager;
		}
	}

	public GraphicsManager getGraphicsManager() throws GraphicsException {
		if (displayGraphicsManagers.size() < defaultDisplay + 1) {
			throw new GraphicsException();
		}
		return displayGraphicsManagers.get(defaultDisplay);
	}

	public GraphicsManager getGraphicsManagerForDisplay(int display) throws GraphicsException {
		if (displayGraphicsManagers.size() < display + 1) {
			throw new GraphicsException();
		}
		return displayGraphicsManagers.get(defaultDisplay);
	}

	public boolean valid() {
		for(GraphicsManager gm: displayGraphicsManagers) {
			if(gm.ready() == false) {
				return false;
			}
		}
		return true;
	}
}
