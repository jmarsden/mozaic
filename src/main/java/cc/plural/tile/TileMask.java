/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.tile;

public class TileMask {
	
	public static enum Mask {
		VISIBLE,
		HIDDEN,
		EDGE;
	}
	
	final int width;
	final int height;
	Mask[][] mask;

	public TileMask(int width, int height) {
		this.width = width;
		this.height = height;
		mask = new Mask[width][height];
		for(int i=0;i<width;i++) {
			for(int j=0;j<height;j++) {
				mask[i][j] = Mask.VISIBLE;
			}
		}
	}
	
	public void setMask(Mask[][] mask) {
		if(mask.length != height || mask[0].length != width) {
			throw new NullPointerException("Mask has wrong dimentions");
		}
		this.mask = mask;
	}
	
	public Mask[][] getMask() {
		return this.mask;
	}
}
