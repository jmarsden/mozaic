/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.tile;

public class FixedWorld extends World implements WorldOfKnownSize {
	
	public final int m;
	public final int n;
	
	public final float worldWidth;
	public final float worldHeight;
	
	/**
	 * Tile backed for the Fixed world.
	 */
	Tile[][] tiles;

	/**
	 * 
	 * @param m
	 * @param n
	 * @param t
	 */
	public FixedWorld(int m, int n, float tileWidth, float tileHeight) {
		super(tileWidth, tileHeight);
		this.m = m;
		this.n = n;

		this.worldWidth = tileWidth * m;
		this.worldHeight = tileHeight * n;
		
		this.tiles = new Tile[m][n];
	}
	
	
	/**
	 * @return the m
	 */
	public int getM() {
		return m;
	}

	/**
	 * @return the n
	 */
	public int getN() {
		return n;
	}
	
	/**
	 * @return the worldWidth
	 */
	public float getWorldWidth() {
		return worldWidth;
	}

	/**
	 * @return the worldHeight
	 */
	public float getWorldHeight() {
		return worldHeight;
	}
	
	public int tileCount() {
		return m * n;
	}
	
	public void setTile(int x, int y, Tile tile) throws WorldBoundsException {
		if(x < 0 || y < 0) {
			throw new WorldBoundsException("Invalid Index (negative)");
		}
		if(x >= m) {
			throw new WorldBoundsException(String.format("x cannot be greater or equal to m (%s > %s)", x, m));
		}
		if(y >= n) {
			throw new WorldBoundsException(String.format("y cannot be greater or equal to n (%s > %s)", y, n));
		}
		this.tiles[x][y] = tile;
		onTileChange(x,y,tile);
	}

	@Override
	public Tile getTile(int x, int y) throws WorldBoundsException {
		if(x < 0 || y < 0) {
			throw new WorldBoundsException("Invalid Index (negative)");
		}
		if(x >= m) {
			throw new WorldBoundsException(String.format("x cannot be greater or equal to m (%s > %s)", x, m));
		}
		if(y >= n) {
			throw new WorldBoundsException(String.format("y cannot be greater or equal to n (%s > %s)", y, n));
		}
		return this.tiles[x][y];
	}
}
