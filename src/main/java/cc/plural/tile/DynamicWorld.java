/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.tile;

public abstract class DynamicWorld extends World implements WorldLoader {

	int xInterest;
	int yInterest;
	int xMin;
	int xMax;
	int yMin;
	int yMax;
	
	int loadWindowSize;
	
	public static final int DEFAULT_LOAD_WINDOW_SIZE;
	
	static {
		DEFAULT_LOAD_WINDOW_SIZE = 20;
	}
	
	public DynamicWorld(float tileWidth, float tileHeight) {
		this(tileWidth, tileHeight, DEFAULT_LOAD_WINDOW_SIZE, 0, 0);
	}
	
	public DynamicWorld(float tileWidth, float tileHeight, int loadWindowSize) {
		this(tileWidth, tileHeight, loadWindowSize, 0, 0);
	}
	
	public DynamicWorld(float tileWidth, float tileHeight, int loadWindowSize, int xInterest, int yInterest) {
		super(tileWidth, tileHeight);
		loadWindowSize = DEFAULT_LOAD_WINDOW_SIZE;
	}

	@Override
	public Tile getTile(int x, int y) throws WorldBoundsException {
		return loadTile(x, y);
	}
	
	public abstract Tile loadTile(int x, int y);

}
