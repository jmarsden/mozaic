/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.tile;

public class DontOverflowWorldFocus extends FocusMode {

	@Override
	public void focus(World world, Portal portal) {

		boolean gtX = false, ltX = false, gtY = false, ltY = false;
		float workingWorldCenterX;
		float workingWorldCenterY;
		synchronized (portal.worldFocus) {
			//System.out.println(portal.worldFocus);
			
			workingWorldCenterX = portal.worldFocus.x;
			workingWorldCenterY = portal.worldFocus.y;
			
			if(world instanceof FixedWorld) {
				FixedWorld fixedWorld = (FixedWorld) world;
				if (portal.worldFocus.x <= portal.halfWidth) {
					ltX = true;
					workingWorldCenterX = portal.halfWidth;
				}
				if (portal.worldFocus.x >= (fixedWorld.worldWidth - portal.halfWidth)) {
					gtX = true;
					workingWorldCenterX = fixedWorld.worldWidth - portal.halfWidth;
				}
				if (portal.worldFocus.y <= portal.halfHeight) {
					ltY = true;
					workingWorldCenterY = portal.halfHeight;
				}
				if (portal.worldFocus.y >= (fixedWorld.worldHeight - portal.halfHeight)) {
					gtY = true;
					workingWorldCenterY = fixedWorld.worldHeight - portal.halfHeight;
				}
			}
			
			if (ltX || gtX || ltY || gtY) {
				portal.worldCenter.setLocation(workingWorldCenterX,
						workingWorldCenterY);
				portal.focus.setLocation(portal.center.x + portal.worldFocus.x
						- workingWorldCenterX, portal.center.y
						+ portal.worldFocus.y - workingWorldCenterY);
			} else {
				portal.worldCenter.setLocation(portal.worldFocus);
				portal.center.setLocation(portal.halfWidth, portal.halfHeight);
				portal.focus.setLocation(portal.center);

			}
			portal.worldBounds.setRect(portal.worldCenter.x - portal.halfWidth,
					portal.worldCenter.y - portal.halfHeight, portal.width,
					portal.height);
		}
	}
}
