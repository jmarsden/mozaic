/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.tile;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

public class Portal {

	public final World world;

	public final float width;

	public final float height;

	public final float halfWidth;

	public final float halfHeight;

	final Point2D.Float center;

	final Point2D.Float worldCenter;

	final Rectangle2D.Float bounds;

	final Rectangle2D.Float worldBounds;

	final Point2D.Float focus;

	final Point2D.Float worldFocus;

	boolean focusDirty;

	FocusMode focusMode;
	
	PortalSnapshot snapshot;

	public Portal(World world, float width, float height) {
		this.world = world;
		this.width = width;
		this.height = height;
		this.halfWidth = width / 2F;
		this.halfHeight = height / 2F;

		center = new Point2D.Float(this.width / 2F, this.height / 2F);
		bounds = new Rectangle2D.Float(0, 0, width, height);

		worldCenter = new Point2D.Float(this.width / 2F, this.height / 2F);
		worldBounds = new Rectangle2D.Float(0, 0, width, height);

		focus = new Point2D.Float();
		worldFocus = new Point2D.Float();

		focusMode = new DontOverflowWorldFocus();
		focusDirty = false;
		
		snapshot = new PortalSnapshot();
	}

	public void setWorldFocus(Point2D focus) {
		worldFocus.setLocation(focus);
		focusDirty = true;
	}

	public void moveWorldFocusNorth(float q) {
		worldFocus.setLocation(worldFocus.getX(), worldFocus.getY() + q);
		focusDirty = true;
	}

	public void moveWorldFocusSouth(float q) {
		worldFocus.setLocation(worldFocus.getX(), worldFocus.getY() - q);
		focusDirty = true;

	}

	public void moveWorldFocusEast(float q) {
		worldFocus.setLocation(worldFocus.getX() + q, worldFocus.getY());
		focusDirty = true;
	}

	public void moveWorldFocusWest(float q) {
		worldFocus.setLocation(worldFocus.getX() - q, worldFocus.getY());
		focusDirty = true;
	}

	public void viewWorld(PortalSnapshot snapshot) {
		if (snapshot == null) {
			throw new NullPointerException("Snapshot cannot be null.");
		}
		if (focusDirty == true) {
			focusMode.focus(world, this);
			focusDirty = false;
		}

		List<PortalPlacement> portalPlacement = new ArrayList<PortalPlacement>();	
		
		int startTileM = 0;
		int endTileM = 0;
		int startTileN = 0;
		int endTileN = 0;
		if(world instanceof WorldOfKnownSize) {
			WorldOfKnownSize knownWorld = (WorldOfKnownSize) world;
			startTileM = (int) Math.max(Math.floor((worldBounds.x / world.tileWidth)),0);
			endTileM = (int) Math.min(Math.floor(((worldBounds.x+worldBounds.width) / world.tileWidth)+1D),knownWorld.getM());
			startTileN = (int) Math.max(Math.floor((worldBounds.y / world.tileHeight)),0);
			endTileN = (int) Math.min(Math.floor(((worldBounds.y+worldBounds.height) / world.tileHeight)+1D),knownWorld.getN());
		} else {
			startTileM = (int) (worldBounds.x / world.tileWidth);
			endTileM = (int) ((worldBounds.x+worldBounds.width) / world.tileWidth) + 1;
			startTileN = (int) (worldBounds.y / world.tileHeight);
			endTileN = (int) ((worldBounds.y+worldBounds.height) / world.tileHeight) + 1;
		}
		
		int xOffset = (int) (worldBounds.x%world.tileWidth);
		int yOffset = (int) (worldBounds.y%world.tileHeight);
		
		int xPlaceStart = -xOffset;
		int yPlaceStart = -yOffset;
		
		float xPlace = xPlaceStart;
		
		for(int i=startTileM;i<endTileM;i++) {
			float yPlace = yPlaceStart;
			for(int j=startTileN;j<endTileN;j++) {
				portalPlacement.add(new PortalPlacement(world.getTile(i, j), xPlace, yPlace));
				yPlace+=world.tileHeight;
			}
			xPlace+=world.tileWidth;
		}
		
		snapshot.portalPlacements = portalPlacement;
		snapshot.focus = focus;
	}
}
