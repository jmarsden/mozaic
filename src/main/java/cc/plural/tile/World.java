/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.tile;

public abstract class World {

	float tileWidth;
	float tileHeight;

	public World(float tileWidth, float tileHeight) {
		this.tileWidth = tileWidth;
		this.tileHeight = tileHeight;
	}

	/**
	 * @return the tileWidth
	 */
	public float getTileWidth() {
		return tileWidth;
	}

	/**
	 * @param tileWidth
	 *            the tileWidth to set
	 */
	public void setTileWidth(float tileWidth) {
		this.tileWidth = tileWidth;
	}

	/**
	 * @return the tileHeight
	 */
	public float getTileHeight() {
		return tileHeight;
	}

	/**
	 * @param tileHeight
	 *            the tileHeight to set
	 */
	public void setTileHeight(float tileHeight) {
		this.tileHeight = tileHeight;
	}

	public abstract Tile getTile(int x, int y) throws WorldBoundsException;

	public void onTileChange(int x, int y, Tile tile) {
	}

	void onInterestInBounds() {

	}

}
