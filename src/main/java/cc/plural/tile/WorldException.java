/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.tile;

public class WorldException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5989286454373216723L;

	/**
	 * 
	 */
	public WorldException() {
		super();
	}

	/**
	 * @param message
	 * @param cause
	 */
	public WorldException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public WorldException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public WorldException(Throwable cause) {
		super(cause);
	}
}
