/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.resource;

/**
 * @author J.W.Marsden <jmarsden@plural.cc>
 */
public class ResourceException extends Exception {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 7027480810034013070L;

	public ResourceException() {
		super();
	}

	public ResourceException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public ResourceException(String message) {
		super(message);
	}

	public ResourceException(Throwable throwable) {
		super(throwable);
	}

}
