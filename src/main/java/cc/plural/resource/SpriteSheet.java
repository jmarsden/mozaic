/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.resource;

/**
 * @author J.W.Marsden <jmarsden@plural.cc>
 */
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

/**
 * Sprite Sheet wrapper for any resource that provides a matrix of sprites.
 * 
 * @author J.W.Marsden
 */
public class SpriteSheet {
	/**
	 * Path to Resource
	 */
	protected String path;

	/**
	 * Storage Image Width
	 */
	protected int width;

	/**
	 * Storage Image Height
	 */
	protected int height;

	/**
	 * BufferedImage store
	 */
	protected BufferedImage store;

	/**
	 * Constructs and ImageStore using the given resource path.
	 * 
	 * @param path The Path to resource.
	 * @throws ResourceException 
	 */
	public SpriteSheet(String path) throws ResourceException {
		if (path == null) {
			throw new NullPointerException("Path to resource cannot be null.");
		}
		this.path = path;
		URL url = this.getClass().getClassLoader().getResource(path);
		if (url == null) {
			throw new ResourceException("Can't find ref: " + path);
		}
		try {
			store = ImageIO.read(url);
			store.flush();
		} catch (IOException throwable) {
			throw new ResourceException("Error Reading Sprite", throwable);
		}
		height = store.getHeight();
		width = store.getWidth();
	}

	/**
	 * Retrieve the Storage Image Height.
	 * 
	 * @return height of image.
	 */
	public int getSheetHeight() {
		return height;
	}

	/**
	 * Retrieve the Storage Image Width.
	 * 
	 * @return width of image.
	 */
	public int getSheetWidth() {
		return width;
	}

	/**
	 * Retrieve the Image from the Sprite Store.
	 * 
	 * @return The full storage image.
	 */
	public BufferedImage getImage() {
		return store;
	}

	/**
	 * Retrieves a buffered image from the image store.
	 * 
	 * @param bounds The rectangular bounds for the image that is to be retrieved.
	 * @return The buffered image instance.
	 */
	public BufferedImage getImage(Rectangle bounds) {
		BufferedImage returnImage = null;	
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice gs = ge.getDefaultScreenDevice();
		GraphicsConfiguration gc = gs.getDefaultConfiguration();
		// Create an image that does not support transparency
		// returnImage = gc.createCompatibleImage(width, height, Transparency.OPAQUE);
		// Create an image that supports transparent pixels
		returnImage = gc.createCompatibleImage(bounds.width, bounds.height, Transparency.BITMASK);
		Graphics2D graphics = returnImage.createGraphics();
		graphics.drawImage(store, 0, 0, 
				(int) bounds.getHeight(), 
				(int) bounds.getWidth(), 
				(int) bounds.getX(), 
				(int) bounds.getY(), 
				(int) (bounds.getX() + bounds.getWidth()), 
				(int) (bounds.getY() + bounds.getHeight()), null);
		graphics.dispose();
		return returnImage;
	}
	
	/**
	 * Retrieves a buffered image from the image store.
	 * 
	 * @param x Top left x coordinate
	 * @param y Top left y coordinate
	 * @param width Width for the image
	 * @param height Height for the image
	 * @return The buffered image.
	 */
	public BufferedImage getImage(float x, float y, float width, float height) {
		BufferedImage returnImage = null;
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice gs = ge.getDefaultScreenDevice();
		GraphicsConfiguration gc = gs.getDefaultConfiguration();
		// Create an image that does not support transparency
		// returnImage = gc.createCompatibleImage(width, height, Transparency.OPAQUE);
		// Create an image that supports transparent pixels
		returnImage = gc.createCompatibleImage((int) width, (int) height, Transparency.BITMASK);
		Graphics2D graphics = returnImage.createGraphics();
		graphics.drawImage(store, 0, 0, 
				(int) height, 
				(int) width, 
				(int) x, 
				(int) y, 
				(int) (x + width), 
				(int) (y + height), null);
		graphics.dispose();
		return returnImage;
	}
	
	/**
	 * Retrieves a buffered image from the image store.
	 * 
	 * @param x Top left x coordinate
	 * @param y Top left y coordinate
	 * @param width Width for the image
	 * @param height Height for the image
	 * @return The buffered image.
	 */
	public BufferedImage getImage(float x, float y, float width, float height, boolean debug) {
		return getImage(x, y, width, height, debug, "" + x + "x" + y);
	}
	
	/**
	 * Retrieves a buffered image from the image store.
	 * 
	 * @param x Top left x coordinate
	 * @param y Top left y coordinate
	 * @param width Width for the image
	 * @param height Height for the image
	 * @return The buffered image.
	 */
	public BufferedImage getImage(float x, float y, float width, float height, boolean debug, String debugText) {
		BufferedImage returnImage = null;
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice gs = ge.getDefaultScreenDevice();
		GraphicsConfiguration gc = gs.getDefaultConfiguration();
		// Create an image that does not support transparency
		// returnImage = gc.createCompatibleImage(width, height, Transparency.OPAQUE);
		// Create an image that supports transparent pixels
		returnImage = gc.createCompatibleImage((int) width, (int) height, Transparency.BITMASK);
		Graphics2D graphics = returnImage.createGraphics();
		graphics.drawImage(store, 0, 0, 
				(int) height, 
				(int) width, 
				(int) x, 
				(int) y, 
				(int) (x + width), 
				(int) (y + height), null);
		if(debug) {
			Font font = new Font("monospace",Font.PLAIN,9);
			graphics.setFont(font);
			graphics.setColor(Color.BLACK);
			graphics.drawLine(0, 0, 0, (int) height);
			graphics.drawLine(0, 0, (int) width, 0);
			graphics.setColor(Color.BLACK);
			graphics.drawString(debugText, 2, 10);
		}
		graphics.dispose();
		return returnImage;
	}
}
