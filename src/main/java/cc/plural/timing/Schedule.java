/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.timing;

import java.util.ArrayList;
import java.util.List;

public abstract class Schedule implements Runnable {

	public static enum Status {
		STARTUP, SLEEPING, RUNNING, COMPLETE, PAUSED;
	}
	
	public final Timer timer;
	
	public Status status;
	
	public ScheduleNotify scheduler;
	
	/**
	 * Vector of actions.
	 */
	protected List<ScheduledAction> actionList;

	
	/**
	 * Constructor for Loop Thread. 
	 * 
	 * @param loopPeriod The period that the loop thread will cycle at.
	 */
	public Schedule() {
		status = Status.STARTUP;
		timer = Timer.getInstance();
		scheduler = null;
		actionList = new ArrayList<ScheduledAction>();
	}

	public void listen(ScheduleNotify scheduler) {
		this.scheduler = scheduler;
	}

	public void runActions() {
		long time = timer.getNanoSecondTime();
		runActions(time);
	}
	
	public void runActions(long forTime) {
		long entryTime = timer.getNanoSecondTime();
		for(int i=0;i<actionList.size();i++) {
			ScheduledAction action = actionList.get(i);
			if(i==0) {
				runAction(forTime, 0, action);
			} else {
				runAction(forTime, timer.getNanoSecondTime()-entryTime, action);
			}
		}
	}
	
	public abstract void runAction(long time, long lag, ScheduledAction action);
}
