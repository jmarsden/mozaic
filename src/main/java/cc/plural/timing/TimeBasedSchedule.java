/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.timing;

import java.util.Date;

public class TimeBasedSchedule extends Schedule implements Runnable {

	final long executeTime;
	
	public TimeBasedSchedule(long executeTime) {
		this.executeTime = executeTime;
		status = Status.STARTUP;
		onStatusChange(status);
	}
	
	public void run() {
		if(status == Status.STARTUP) {
			status = Status.RUNNING;
			onStatusChange(status);
		} else {
			return;
		}
		long now = Timer.getInstance().getMilliSecondDateTime();
		long sleepTime = executeTime - now;
		if(sleepTime <= 0) {
			runActions();
		} else {
			try {
				status = Status.SLEEPING;
				onStatusChange(status);
				Thread.sleep(sleepTime);
				status = Status.RUNNING;
				onStatusChange(status);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			runActions();
		}	
		status = Status.COMPLETE;
		onStatusChange(status);
		onComplete();
	}

	@Override
	public void runAction(long time, long lag, ScheduledAction action) {
		try {
			action.execute(time, lag);
		} catch (ScheduledActionExecuteException e) {
			e.printStackTrace();
		}
	}
	
	private void onStatusChange(Status status) {
		if(scheduler != null) {
			scheduler.scheduleStatusChange(this, status, Timer.getInstance().getMilliSecondDateTime());
		}
		
	}
	
	private void onComplete() {
		if(scheduler != null) {
			scheduler.scheduleComplete(this, Timer.getInstance().getMilliSecondDateTime());
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TimeBasedSchedule [Run " + actionList.size() + " actions at " + new Date(executeTime) + "]";
	}
	
	
}
