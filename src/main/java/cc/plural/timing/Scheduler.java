/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.timing;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cc.plural.timing.Schedule.Status;

public class Scheduler implements ScheduleNotify {

	public final static Scheduler instance;

	List<Schedule> schedules;

	static {
		instance = new Scheduler();
	}

	public Scheduler() {
		schedules = new ArrayList<Schedule>();
	}

	public void reset() {
		for (Schedule schedule : schedules) {
			schedule.status = Status.COMPLETE;
		}
		schedules = new ArrayList<Schedule>();
	}

	public void scheduleDelayedAction(ScheduledAction action, long milliSecondDelay) {
		scheduleAction(action, Timer.getInstance().getMilliSecondDateTime() + milliSecondDelay);
	}

	public void scheduleAction(ScheduledAction action, long milliSecondTime) {
		for (Schedule schedule : schedules) {
			if (schedule instanceof TimeBasedSchedule) {
				TimeBasedSchedule timeBasedSchedule = (TimeBasedSchedule) schedule;
				if (timeBasedSchedule.executeTime == milliSecondTime) {
					timeBasedSchedule.actionList.add(action);
					return;
				}
			}
		}
		synchronized (schedules) {
			TimeBasedSchedule timeBasedSchedule = new TimeBasedSchedule(milliSecondTime);
			timeBasedSchedule.actionList.add(action);
			schedules.add(timeBasedSchedule);
			kickStart(timeBasedSchedule);
		}
	}

	public void scheduleAction(ScheduledAction action, Date date) {
		scheduleAction(action, date.getTime());
	}

	public void scheduleRepeatingAction(ScheduledAction action, int period) {
		synchronized (schedules) {
			for (Schedule schedule : schedules) {
				if (schedule instanceof RepeatingSchedule) {
					RepeatingSchedule repeatingSchedule = (RepeatingSchedule) schedule;
					int basePeriod = repeatingSchedule.getBasePeriod();
					if ((period % basePeriod) == 0) {
						int sequence = period / basePeriod;
						System.out.println("Adding to RepeatingSchedule:" + repeatingSchedule + " at sequence:" + sequence);
						repeatingSchedule.addScheduledAction(action, sequence);
						return;
					}
				}
			}
			RepeatingSchedule repeatingSchedule = new RepeatingSchedule(period);
			int sequence = 0;
			repeatingSchedule.addScheduledAction(action, sequence);
			schedules.add(repeatingSchedule);
			kickStart(repeatingSchedule);
		}
	}

	public void scheduleRepeatingAction(ScheduledAction action, int period, int count) {

	}

	public void unscheduleAction(ScheduledAction action) {

	}

	private void kickStart(Schedule schedule) {
		schedule.listen(this);
		(new Thread(schedule)).start();
	}

	public void scheduleStatusChange(Schedule schedule, Status status, long time) {

	}

	public void scheduleComplete(Schedule schedule, long time) {
		System.out.println("ScheduleComplete... Remove Me. " + schedule + " " + new java.util.Date(time));
		synchronized (schedules) {
			schedules.remove(schedule);
		}
	}
}