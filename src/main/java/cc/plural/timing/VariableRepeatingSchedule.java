/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.timing;

import java.util.HashMap;
import java.util.Map;

public class VariableRepeatingSchedule extends Schedule implements Runnable {
	
	Map<ScheduledAction, RepeatingScheduledActionData> actionData;
	
	protected long sequence;
	
	/**
	 * Holds the period of the loop thread.
	 */
	protected int basePeriodMilli;
	
	/**
	 * Holds the start time of the Thread.
	 */
	protected long loopStartTimeMilli;
	
	/**
	 * Holds the nano time required to adjust the next sleep by due to 
	 * the previous loops time errors.
	 * 
	 * This is due to the time error in Java's Thread.sleep() method and the
	 * differences in times the actions can take.
	 */
	protected long loopErrorAdjustNano = 0;
	
	/**
	 * Holds the total adjustment required to the current loops sleep time
	 * based on time to execute actions.
	 */
	protected long loopSleepTotalAdjustNano = 0;
	
	/**
	 * Holds the nano time that the Thread will be requested to sleep in 
	 * this iteration of the game loop.
	 */
	protected long loopSleepTimeNano = 0;
	
	public static final int DEFAULT_LOOP_PERIOD_MILLIS;
	
	public static final int MINIMUM_SLEEP_TIME_MILLIS;
	
	static {
		DEFAULT_LOOP_PERIOD_MILLIS = 100;
		MINIMUM_SLEEP_TIME_MILLIS = 5;
	}
	
	/**
	 * Constructor for Loop Thread. 
	 * 
	 * @param loopPeriod The period that the loop thread will cycle at.
	 */
	public VariableRepeatingSchedule() {
		super();
		sequence = 0;
		status = Status.STARTUP;
		basePeriodMilli = DEFAULT_LOOP_PERIOD_MILLIS;
		actionData = new HashMap<ScheduledAction, RepeatingScheduledActionData>();
	}
	
	public VariableRepeatingSchedule(int basePeriod) {
		super();
		sequence = 0;
		status = Status.STARTUP;
		basePeriodMilli = basePeriod;
		actionData = new HashMap<ScheduledAction, RepeatingScheduledActionData>();
	}

	
	/**
	 * Access Method for base period.
	 * 
	 * @return The base period.
	 */
	public int getBasePeriod() {
		return basePeriodMilli;
	}
	
	public void run() {
		if(status == Status.STARTUP) {
			status = Status.RUNNING;
			onStatusChange(status);
		} else {
			return;
		}
		// StopWatches
		StopWatch actionWatch = new StopWatch();
		StopWatch loopWatch = new StopWatch();
		StopWatch sleepWatch = new StopWatch();
		
		// Set the Loop Start Time
		loopStartTimeMilli = Timer.getInstance().getMilliSecondDateTime();
		
		while(status != Status.COMPLETE) {
			// Check Paused
			while(status == Status.PAUSED) {
				try {
					status.wait();
				} catch (InterruptedException e) { }
			}		
			
			if(status == Status.COMPLETE) {
				break;
			}
			
			// Start the loop timer.
			loopWatch.start();
			// Start the action timer.
			actionWatch.start();
			
			// Do actions
			runActions(Timer.getInstance().getMilliSecondDateTime());
			
			// Stop the Action Timer.
			actionWatch.stop();
			
			// Calculate the total adjustment time.
			loopSleepTotalAdjustNano = actionWatch.getDeltaNanoTime() + loopErrorAdjustNano;
			// Find the total sleep time.
			loopSleepTimeNano = Timer.milliSecondsToNanoSeconds(getBasePeriod()) - loopSleepTotalAdjustNano;

			// Start the sleep timer.
			sleepWatch.start();
			
			// Sleep the thread.
			int sleepTimeMilli = (int) Timer.nanoSecondsToMilliSeconds(loopSleepTimeNano);
			if(sleepTimeMilli < 0) {
				sleepTimeMilli = MINIMUM_SLEEP_TIME_MILLIS;
			}
			try {
				status = Status.SLEEPING;
				onStatusChange(status);
				Thread.sleep(sleepTimeMilli);
				if(status == Status.COMPLETE) {
					break;
				}
				status = Status.RUNNING;
				onStatusChange(status);
			} catch (InterruptedException e) { 
				if(status == Status.COMPLETE) {
					break;
				}
			}
		
			// Stop the sleep timer.
			sleepWatch.stop();
			// Stop the loop timer.
			loopWatch.stop();
			
			// Calculate the loop time error adjust so we can adjust the next loop.
			loopErrorAdjustNano = Timer.milliSecondsToNanoSeconds(getBasePeriod()) - loopErrorAdjustNano - loopWatch.getDeltaNanoTime();
		
			/*
			@SuppressWarnings("unused")
			String timeStringOutput="";
			timeStringOutput += "ActionTime:" + Timer.nanoSecondsToReadableMilliSeconds(actionWatch.getDeltaNanoTime()) + " ";
			timeStringOutput += "SleepTimeTarget:" + Timer.nanoSecondsToReadableMilliSeconds(loopSleepTimeNano) + " ";
			timeStringOutput += "SleepTimeActual:" + Timer.nanoSecondsToReadableMilliSeconds(sleepWatch.getDeltaNanoTime()) + " ";
			timeStringOutput += "LoopTime:" + Timer.nanoSecondsToReadableMilliSeconds(loopWatch.getDeltaNanoTime()) + " ";
			timeStringOutput += "ErrorTime:" + Timer.nanoSecondsToReadableMilliSeconds(loopErrorAdjustNano) + " ";
			
			System.out.println(timeStringOutput);
			*/
			sequence++;
		}
		onComplete();
	}
	
	@Override
	public synchronized void runActions(long time) {
			for(ScheduledAction action: actionList) {
				RepeatingScheduledActionData ad = actionData.get(action);
				if(ad.sequence == 0 || sequence%ad.sequence == 0) {
					runAction(time, 0, action);
				}
			}
	}
	
	@Override
	public void runAction(long time, long lag, ScheduledAction action) {
		try {
			action.execute(time, 0);
		} catch (ScheduledActionExecuteException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void addScheduledAction(ScheduledAction action, int sequence) {
		if(actionList.contains(action)) {
			return;
		}
		actionData.put(action, new RepeatingScheduledActionData(sequence));
		actionList.add(action);
	}
	
	public void pause() throws TimingException {
		if(status != Status.RUNNING || status != Status.SLEEPING) {
			throw new TimingException();
		}
		status = Status.PAUSED;
	}
	
	public void unPause() throws TimingException {
		if(status != Status.PAUSED) {
			throw new TimingException();
		}
		status = Status.RUNNING;
	}
	
	private void onStatusChange(Status status) {
		if(scheduler != null) {
			scheduler.scheduleStatusChange(this, status, Timer.getInstance().getMilliSecondDateTime());
		}
		
	}
	
	private void onComplete() {
		if(scheduler != null) {
			scheduler.scheduleComplete(this, Timer.getInstance().getMilliSecondDateTime());
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getClass().getName() + "@" + Integer.toHexString(hashCode());
	}

	
	public static class RepeatingScheduledActionData {
		
		public int sequence;
		
		public RepeatingScheduledActionData() {
			sequence = 0;
		}
		
		public RepeatingScheduledActionData(int sequence) {
			this.sequence = sequence;
		}
		
	}
}
