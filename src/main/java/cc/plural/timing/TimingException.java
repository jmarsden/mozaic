/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.timing;

/**
 * Timer Exception. General Exception thrown in the timing package.
 * 
 * @author J.W.Marsden <jmarsden@plural.cc>
 */
public class TimingException extends Exception {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 3289370298917890430L;

	public TimingException() {
		super();
	}
	
	public TimingException(String message) {
		super(message);
	}
	
	public TimingException(Throwable throwable) {
		super(throwable);
	}
	
	public TimingException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
