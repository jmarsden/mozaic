/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.timing;

/**
 * Stop watch - acts like a stop watch. 
 * 
 * @author J.W.Marsden <jmarsden@plural.cc>
 */
public class StopWatch {

	public long startNanoTime;
	public long stopNanoTime;
	public long deltaNanoTime;
	public long cumulativeNanoTime;
	
	public StopWatch() {
		startNanoTime = -1;
		stopNanoTime = -1;
		deltaNanoTime = -1;
		cumulativeNanoTime = -1;
	}
	
	long setStartNanoTime(long startNanoTime) {
		return this.startNanoTime = startNanoTime;
	}
	
	public long getStartNanoTime() {
		return this.startNanoTime;
	}
	
	long setStopNanoTime(long stopNanoTime) {
		return this.stopNanoTime = stopNanoTime;
	}
	
	public long getStopNanoTime() {
		return this.stopNanoTime;
	}
	
	long setDeltaNanoTime(long deltaNanoTime) {
		return this.deltaNanoTime = deltaNanoTime;
	}
	
	public long getDeltaNanoTime() {
		return this.deltaNanoTime;
	}
	
	long setCumulativeNanoTime(long cumulativeNanoTime) {
		return this.cumulativeNanoTime = cumulativeNanoTime;
	}
	
	long adjustCumulativeNanoTime(long time) {
		return this.cumulativeNanoTime += time;
	}
	
	public long getCumulativeNanoTime() {
		return this.cumulativeNanoTime;
	}
	
	public synchronized long start() {
		setStartNanoTime(Timer.getInstance().getNanoSecondTime());
		return 0L;
	}
	
	public synchronized long stop() {
		setDeltaNanoTime(setStopNanoTime(Timer.getInstance().getNanoSecondTime())-getStartNanoTime());
		adjustCumulativeNanoTime(getDeltaNanoTime());
		return getDeltaNanoTime();
	}
	
	public synchronized long reset() {
		return setCumulativeNanoTime(0L);
	}
	
	public long time() {
		return getCumulativeNanoTime();
	}
}
