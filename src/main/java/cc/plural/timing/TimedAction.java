/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.timing;

import java.lang.reflect.Array;

/**
 * Base class for actions run on a timed schedule.
 * 
 * @author J.W.Marsden <jmarsden@plural.cc>
 */
public abstract class TimedAction extends ScheduledAction {
	
	protected int executionTimeHistorySize;
	
	protected StopWatch actionPayloadWatch;
	
	/**
	 * Count of pulses. The number of times this action has been called.
	 */
	protected long pulseCount;
	
	/**
	 * Current frame that the storage arrays at
	 */
	protected int payloadFrame;
	
	/**
	 * Container for action execution start time nano's. These are only meaningfull
	 * in respect to each other as the reference time is meaningless.
	 */
	protected long[] payloadStartTimeHistory;
	/**
	 * Container for action delta (execution) time nano's
	 */
	protected long[] payloadDeltaTimeHistory;
	
	public final static int DEFAULT_HISTORY_SIZE;

	static {
		DEFAULT_HISTORY_SIZE = 20;
	}
	
	public TimedAction() {
		setExecutionTimeHistorySize(DEFAULT_HISTORY_SIZE);
		setActionPayloadWatch(new StopWatch());
	}
	
	public long getPulse() {
		return pulseCount;
	}

	public long setPulse(int pulseCount) {
		return this.pulseCount = pulseCount;
	}

	public long pulse() {
		return this.pulseCount = this.pulseCount+1;
	}

	protected synchronized int getPayloadFrame() {
		return payloadFrame;
	}

	protected synchronized int setPayloadFrame(int payloadFrame) {
		return this.payloadFrame = payloadFrame;
	}
	
	/**
	 * Steps the Payload Frame based on the internal size of the Payload Frame.
	 * 
	 * @return The new Payload Frame
	 */
	protected synchronized int stepPayloadFrame() {
		this.payloadFrame = this.payloadFrame+1;
		if(payloadFrame >= getExecutionTimeHistorySize()) {
			this.payloadFrame = 0;
		}
		return this.payloadFrame;
	}

	public synchronized int getExecutionTimeHistorySize() {
		return executionTimeHistorySize;
	}

	public synchronized void setExecutionTimeHistorySize(int executionTimeHistorySize) {
		this.executionTimeHistorySize = executionTimeHistorySize;
		payloadStartTimeHistory = new long[getExecutionTimeHistorySize()];
		payloadDeltaTimeHistory = new long[getExecutionTimeHistorySize()];
		setPayloadFrame(0);
		setPulse(0);
	}

	public StopWatch getActionPayloadWatch() {
		return actionPayloadWatch;
	}

	public void setActionPayloadWatch(StopWatch actionPayloadWatch) {
		this.actionPayloadWatch = actionPayloadWatch;
	}

	public String getName() {
		return getClass().getName();
	}
	
	void execute(long time, long lag) throws ScheduledActionExecuteException {		
		// Hit Start Stopwatch
		getActionPayloadWatch().start();
		
		// Run Action
		handleExecute(time, lag);
		pulse();
		
		// Hit Stop Stopwatch
		getActionPayloadWatch().stop();
		
		/**
		 * Record Statistics
		 */
		synchronized(this) {
			// Action Pay Load Start Time
			payloadStartTimeHistory[getPayloadFrame()] = getActionPayloadWatch().getStartNanoTime();
			// Action Pay Load Delta Time
			payloadDeltaTimeHistory[getPayloadFrame()] = getActionPayloadWatch().getDeltaNanoTime();
			// Step the Pay Load Frame
			stepPayloadFrame();
		}
	}
	
	protected abstract void handleExecute(long time, long lag) throws ScheduledActionExecuteException; 
	
	/**
	 * Calculates the Average time in nano seconds that it 
	 * takes to process this actions pay load.
	 * 
	 * @return Average Pay Load Period in Nano Seconds.
	 */
	public long getAvgPayloadPeriodTimeNano() {
		long returnData = 0;
		long sumData = 0;
		long[] payloadPeriodTimes = getAllPayloadStartTimesNano();
		int payloadPeriodTimesSize = Array.getLength(payloadPeriodTimes);
		if(payloadPeriodTimesSize>1) {
			for(int i=0;i<payloadPeriodTimesSize-1;i++) {
				sumData = sumData + (payloadPeriodTimes[i]-payloadPeriodTimes[i+1]);
			}
			returnData = sumData/(payloadPeriodTimesSize-1);
		} else {
			returnData = -1;
		}
		return returnData;
	}
	
	public long[] getAllPayloadStartTimesNano() {
		int numberOfValues = (int) Math.min(getExecutionTimeHistorySize(), getPulse());
		int currentFrame = getPayloadFrame();
		
		long[] returnData = new long[numberOfValues];
		
		for(int i=0;i<numberOfValues;i++) {
			currentFrame--;
			if(currentFrame < 0) {
				currentFrame = numberOfValues-1;
			} 
			returnData[i] = payloadStartTimeHistory[currentFrame];
		}
		return returnData;
	}
	
	public long getAvgPayloadDeltaTimeNano() {
		long returnData = 0;
		long sumData = 0;
		long[] payloadDeltaTimes = getAllPayloadDeltaTimesNano();
		int payloadDeltaTimesSize = Array.getLength(payloadDeltaTimes);
		if(payloadDeltaTimesSize>0) {
			for(int i=0;i<payloadDeltaTimesSize;i++) {
				sumData = sumData + payloadDeltaTimes[i];
			}
			returnData = sumData/payloadDeltaTimesSize;
		} else {
			returnData = -1;
		}
		return returnData;
	}
	
	public long[] getAllPayloadDeltaTimesNano() {
		int numberOfValues = (int) Math.min(getExecutionTimeHistorySize(), getPulse());
		int currentFrame = getPayloadFrame();
		
		long[] returnData = new long[numberOfValues];
		
		for(int i=0;i<numberOfValues;i++) {
			currentFrame--;
			if(currentFrame < 0) {
				currentFrame = numberOfValues-1;
			}
			returnData[i] = payloadDeltaTimeHistory[currentFrame];
		}
		return returnData;
	}
}
