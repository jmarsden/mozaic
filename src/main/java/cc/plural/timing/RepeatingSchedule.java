/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.timing;

import java.util.HashMap;
import java.util.Map;

public class RepeatingSchedule extends Schedule implements Runnable {

	public long sequence;

	/**
	 * Holds the period of the loop thread.
	 */
	private int basePeriodMilli;

	private float fudge;

	private Map<ScheduledAction, RepeatingScheduledActionData> actionData;

	private long lastActionPeriod;
	
	public static final int DEFAULT_LOOP_PERIOD_MILLIS;

	static {
		DEFAULT_LOOP_PERIOD_MILLIS = 100;
	}

	/**
	 * Constructor for Loop Thread.
	 * 
	 * @param loopPeriod The period that the loop thread will cycle at.
	 */
	public RepeatingSchedule() {
		super();
		sequence = 0;
		status = Status.STARTUP;
		basePeriodMilli = DEFAULT_LOOP_PERIOD_MILLIS;
		if (basePeriodMilli <= 5) {
			fudge = 0.5F;
		} else if (basePeriodMilli <= 30) {
			fudge = 1F;
		} else if (basePeriodMilli <= 50) {
			fudge = 1.125F;
		} else if (basePeriodMilli > 50) {
			fudge = 1.25F;
		}
		actionData = new HashMap<ScheduledAction, RepeatingScheduledActionData>();
		lastActionPeriod = 0;
	}

	public RepeatingSchedule(int basePeriod) {
		super();
		sequence = 0;
		status = Status.STARTUP;
		basePeriodMilli = basePeriod;
		if (basePeriodMilli <= 5) {
			fudge = 0.5F;
		} else if (basePeriodMilli <= 30) {
			fudge = 1F;
		} else if (basePeriodMilli <= 50) {
			fudge = 1.125F;
		} else if (basePeriodMilli > 50) {
			fudge = 1.25F;
		}
		actionData = new HashMap<ScheduledAction, RepeatingScheduledActionData>();
		lastActionPeriod = 0;
	}

	/**
	 * Access Method for base period.
	 * 
	 * @return The base period.
	 */
	public int getBasePeriod() {
		return basePeriodMilli;
	}

	public synchronized void addScheduledAction(ScheduledAction action, int sequence) {
		if (actionList.contains(action)) {
			return;
		}
		actionData.put(action, new RepeatingScheduledActionData(sequence));
		actionList.add(action);
	}

	public void run() {
		if (status == Status.STARTUP) {
			status = Status.RUNNING;
			onStatusChange(status);
		} else {
			return;
		}
		// StopWatches
		final StopWatch actionWatch = new StopWatch();
		final StopWatch loopWatch = new StopWatch();

		long lastActionTime = System.nanoTime();

		while (status != Status.COMPLETE) {

			// Start the loop timer.
			loopWatch.start();

			// Check Paused
			while (status == Status.PAUSED) {
				try {
					status.wait();
				} catch (InterruptedException e) {
				}
			}

			if (status == Status.COMPLETE) {
				break;
			}

			long systemTime = System.nanoTime();

			// Catch Up
			while (systemTime - lastActionTime > Timer.NANO_MILLI_FACTOR * basePeriodMilli) {
				lastActionTime += Timer.NANO_MILLI_FACTOR * basePeriodMilli;
				actionWatch.start();
				System.out.println("Catchup!");
				runActions(lastActionTime);
				sequence++;

				actionWatch.stop();
			}

			while ((systemTime - lastActionTime) < Timer.NANO_MILLI_FACTOR * (basePeriodMilli - fudge)) {
				Thread.yield();
				try {
					Thread.sleep(1);
				} catch (Exception e) {
				}
				systemTime = System.nanoTime();
			}

			actionWatch.start();

			// Do actions
			// System.out.println("Normal!");
			runActions();
			sequence++;

			// Stop the Action Timer.
			actionWatch.stop();

			lastActionTime = System.nanoTime();

			// Stop the loop timer.
			loopWatch.stop();
		}
		onComplete();
	}

	@Override
	public void runActions(long forTime) {
		long entryTime = timer.getNanoSecondTime();
		for (int i = 0; i < actionList.size(); i++) {
			ScheduledAction action = actionList.get(i);
			RepeatingScheduledActionData ad = actionData.get(action);
			if (ad.sequence == 0 || sequence % ad.sequence == 0) {
					long lag = timer.getNanoSecondTime() - entryTime;
					//System.out.println("Running Action:" + action.toString());
					if (i == 0) {
						runAction(forTime, 0, action);
					} else {
						runAction(forTime, lag, action);
					}
			}

		}
		long finshTime = timer.getNanoSecondTime();
		lastActionPeriod = finshTime-entryTime;
		long catchup = lastActionPeriod - (basePeriodMilli*Timer.NANO_MILLI_FACTOR);
		while(catchup >= basePeriodMilli*Timer.NANO_MILLI_FACTOR) {
			forTime = forTime + basePeriodMilli*Timer.NANO_MILLI_FACTOR;
			for (int i = 0; i < actionList.size(); i++) {
				ScheduledAction action = actionList.get(i);
				RepeatingScheduledActionData ad = actionData.get(action);
				if (action.priority == 10  && (ad.sequence == 0 || sequence % ad.sequence == 0)) {
					runAction(forTime, 0, action);
				}
			}
			sequence++;
			System.out.println("Dropped Frame: " + lastActionPeriod);
			catchup-=basePeriodMilli*Timer.NANO_MILLI_FACTOR;
		}
	}

	@Override
	public void runAction(long time, long lag, ScheduledAction action) {
		try {
			action.execute(time, lag);
		} catch (ScheduledActionExecuteException e) {
			e.printStackTrace();
		}
	}

	public void pause() throws TimingException {
		if (status != Status.RUNNING || status != Status.SLEEPING) {
			throw new TimingException();
		}
		status = Status.PAUSED;
	}

	public void unPause() throws TimingException {
		if (status != Status.PAUSED) {
			throw new TimingException();
		}
		status = Status.RUNNING;
	}

	private void onStatusChange(Status status) {
		if (scheduler != null) {
			scheduler.scheduleStatusChange(this, status, Timer.getInstance().getMilliSecondDateTime());
		}

	}

	private void onComplete() {
		if (scheduler != null) {
			scheduler.scheduleComplete(this, Timer.getInstance().getMilliSecondDateTime());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getClass().getName() + "@" + Integer.toHexString(hashCode());
	}

	public static class RepeatingScheduledActionData {

		public int sequence;

		public RepeatingScheduledActionData() {
			sequence = 0;
		}

		public RepeatingScheduledActionData(int sequence) {
			this.sequence = sequence;
		}

	}
}
