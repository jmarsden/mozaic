/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.timing;

import java.util.Date;

/**
 * Timer Utility Class. Provides all the required time based functionality.
 * 
 * @author J.W.Marsden <jmarsden@plural.cc>
 */
public abstract class Timer {
	/**
	 * Nano Second to Milli Second Conversion factor
	 */
	public static final int NANO_MILLI_FACTOR = 1000000;
	
	public static final double NANO_MILLI_FACTOR_DOUBLE = 1000000D;
	
	/**
	 * The calculated date error. This is the minimum
	 * time between two dates. This is important because
	 * the JVM Date/Time tends to jump all over the place.
	 */
	protected long dateError;
	/**
	 * The reference date/time.
	 */
	protected long refrenceDateTime;
	/**
	 * The reference nano timer.
	 */
	protected long refrenceNanoTime;
	
	/**
	 * Singleton instance of timer
	 */
	protected static Timer timerInstance;
	
	/**
	 * Default Constructor
	 */
	Timer() {
		setRefrenceData();
	}
	
	/**
	 * Retrieves the Timer Singleton Instance.
	 * 
	 * @return Timer Instance.
	 */
	public static Timer getInstance() {
		if(timerInstance == null) {
			timerInstance = getTimerInstance();
		}
		return timerInstance;
	}
	
	/**
	 * Retrives a/the concrete instance of Timer.
	 * 
	 * @return Timer
	 */
	private static Timer getTimerInstance() {
		return new SystemTimer();
	}

	/**
	 * Sets all the required reference information for this timer
	 * instance.
	 * 
	 * After calling this method the Timer will have a valid date 
	 * error, reference date and reference nano time.
	 */
	void setRefrenceData() {
		Date dateOne = null;
		Date dateTwo = null;
		long refrenceNanoTime = 0;
		
		do {
			dateOne = new Date();
			dateTwo = new Date();
			refrenceNanoTime = getNanoSecondTime();
		} while(dateOne.getTime()==dateTwo.getTime());
		
		dateError = dateTwo.getTime() - dateOne.getTime();
		setRefrenceDateTime(dateTwo.getTime());
		setRefrenceNanoTime(refrenceNanoTime);
	}
	
	public long getDateError() {
		return this.dateError;
	}
	
	long setRefrenceNanoTime(long refrenceNanoTime) {
		return this.refrenceNanoTime = refrenceNanoTime;
	}
	
	public long getRefrenceNanoTime() {
		return this.refrenceNanoTime;
	}
	
	long setRefrenceDateTime(long refrenceDateTime) {
		return this.refrenceDateTime = refrenceDateTime;
	}
	
	public long getRefrenceDateTime() {
		return this.refrenceDateTime;
	}
	
	public Date getRefrenceDate() {
		return new Date(refrenceDateTime + getMilliSecondDateTime());
	}
	
	public abstract long getNanoSecondTime();

	public long getMilliSecondTime() {
		long currentNanoTime = getNanoSecondTime();
		return nanoSecondsToMilliSeconds(currentNanoTime);
	}
	
	/**
	 * Retrieves the best attempt at a correct millisecond
	 * date/time long.
	 * 
	 * This method will pull the number of nano seconds since
	 * the reference date/time and the current date time from
	 * the JVM. If the values are out in either direction by the
	 * maximum date error then we need to re-fetch reference 
	 * information.
	 * 
	 * @return Date/Time long
	 */
	public long getMilliSecondDateTime() {
		long currentNanoTime = getNanoSecondTime();
		long deltaTime = currentNanoTime - getRefrenceNanoTime();
		long currentMilliTime = getRefrenceDateTime() + nanoSecondsToMilliSeconds(deltaTime);
		long currentDateTime = new Date().getTime();
		if(Math.abs(currentDateTime - currentMilliTime) > dateError) {
			setRefrenceData();
			return currentDateTime;
		} else {
			return currentMilliTime;
		}		 
	}
	
	public static long nanoSecondsToMilliSeconds(long nanoSeconds) {
		return nanoSeconds/NANO_MILLI_FACTOR;
	}
	
	public static long milliSecondsToNanoSeconds(long milliSeconds) {
		return milliSeconds*NANO_MILLI_FACTOR;
	}
	
	public static float nanoSecondsToReadableMilliSeconds(long nanoSeconds) {
		return nanoSeconds/(float)NANO_MILLI_FACTOR;
	}
	
	public static float nanoSecondsToMilliSeconds(float nanoSeconds) {
		return nanoSeconds/(float)NANO_MILLI_FACTOR;
	}
}
