/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.timing;

/**
 * Basic implementation of Timer. This uses the nano timer 
 * added in java 1.5 but exists if someone ever wants to go back
 * to Java 1.4 or before (you will need to make a native implementation
 * and good luck to you - let me know if you do).
 * 
 * @author J.W.Marsden <jmarsden@plural.cc>
 */
public class SystemTimer extends Timer {
	/**
	 * Retrieve the System Timer.
	 */
	public long getNanoSecondTime() {
		return System.nanoTime();
	}
}
