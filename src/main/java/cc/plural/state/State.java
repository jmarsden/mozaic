/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.state;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import cc.plural.display.Display;
import cc.plural.timing.Scheduler;

public abstract class State implements ComponentListener {

	StateCheck check;
	Display display;
	Scheduler scheduler;
	
	public State() {
		check = null;
		display = null;
		scheduler = null;
	}
	
	public void initStateCheck() {
		if(check == null) {
			check = new StateCheck();
		}	
	}
	
	public void initDisplay() {
		if(display == null) {
			display = new Display();
		}	
	}
	
	public void initScheduler() {
		if(scheduler == null) {
			scheduler = new Scheduler();
		}	
	}
	
	public void resetStateCheck() {
		if(check != null) {
			check = new StateCheck();
		}
	}
	
	public void resetDisplay() {
		if(display != null) {
			display = new Display();
		}	
	}
	
	public void resetScheduler() {
		if(scheduler != null) {
			scheduler = new Scheduler();
		}	
	}
	
	public StateCheck getStateCheck() {
		return check;
	}
	
	public Display getDisplay() {
		return display;
	}
	
	public abstract void init();
	
	public abstract void tick();
	
	public void componentResized(ComponentEvent e) {
	}

	public void componentMoved(ComponentEvent e) {
	}

	public void componentShown(ComponentEvent e) {
	}

	public void componentHidden(ComponentEvent e) {
	}
	
}
