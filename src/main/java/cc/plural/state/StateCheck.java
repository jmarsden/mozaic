/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.state;

import java.util.ArrayList;
import java.util.List;

public class StateCheck {

	private List<StateCheckCase> tests;
	
	public StateCheck() {
		tests = new ArrayList<StateCheckCase>();
	}
	
	public synchronized void addTest(StateCheckCase test) {
		if(test == null) {
			throw new NullPointerException();
		}
		if(tests.contains(test)) {
			tests.remove(test);
		}
		tests.add(test);
	}
	
	public synchronized boolean valid() {
		for(StateCheckCase test: tests) {
			if(!test.valid()) {
				return false;
			}
		}
		return true;
	}
	
	public synchronized void reset() {
		for(StateCheckCase test: tests) {
			test.reset();
		}
	}
}
