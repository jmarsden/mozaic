package cc.plural.resource;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import junit.framework.TestCase;

public class SpriteSheetTest extends TestCase {

	SpriteSheet sheet = null;

	public void testGetSheetHeight() throws ResourceException {
		sheet = new SpriteSheet("base.png");
		assertEquals(480F, sheet.getSheetHeight(), 0F);
	}

	public void testGetSheetWidth() throws ResourceException {
		sheet = new SpriteSheet("base.png");
		assertEquals(480F, sheet.getSheetWidth(), 0F);
	}

	public void testGetImage() throws ResourceException {
		sheet = new SpriteSheet("base.png");
		assertNotNull(sheet);
	}

	public void testGetImageRectangle() throws ResourceException {
		sheet = new SpriteSheet("base.png");
		BufferedImage image = sheet.getImage(new Rectangle(0,0,12,12));
		assertEquals(12F, image.getWidth(), 0F);
		assertEquals(12F, image.getHeight(), 0F);
		
	}

}
