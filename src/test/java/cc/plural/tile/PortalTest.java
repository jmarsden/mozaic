/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.tile;

import java.awt.geom.Point2D;

import junit.framework.TestCase;


/**
 * @author J.W.Marsden <jmarsden@plural.cc>
 */
public class PortalTest extends TestCase {

	World world;
	Portal portal;
	
	/**
	 * @throws java.lang.Exception
	 */
	public void setUp() throws Exception {
		world = new FixedWorld(10,10,10,10);
		portal = new Portal(world,30,20);
	}

	/**
	 * @throws java.lang.Exception
	 */
	public void tearDown() throws Exception {
		portal = null;
		world = null;
	}

	/**
	 * Test method for {@link cc.plural.tile.Portal#moveWorldFocusNorth(float)}.
	 */
	public void testMoveWorldFocusNorth() {
		System.out.println("testMoveWorldFocusNorth");
		portal.setWorldFocus(new Point2D.Float(50F,50F));
		portal.moveWorldFocusNorth(1);
		assertEquals(50F, portal.worldFocus.x, 0);
		assertEquals(51F, portal.worldFocus.y, 0);
	}

	/**
	 * Test method for {@link cc.plural.tile.Portal#moveWorldFocusSouth(float)}.
	 */
	public void testMoveWorldFocusSouth() {
		portal.setWorldFocus(new Point2D.Float(50F,50F));
		portal.moveWorldFocusSouth(1);
		assertEquals(50F, portal.worldFocus.x, 0);
		assertEquals(49F, portal.worldFocus.y, 0);
	}

	/**
	 * Test method for {@link cc.plural.tile.Portal#moveWorldFocusEast(float)}.
	 */
	public void testMoveWorldFocusEast() {
		portal.setWorldFocus(new Point2D.Float(50F,50F));
		portal.moveWorldFocusEast(1);
		assertEquals(51F, portal.worldFocus.x, 0);
		assertEquals(50F, portal.worldFocus.y, 0);
	}

	/**
	 * Test method for {@link cc.plural.tile.Portal#moveWorldFocusWest(float)}.
	 */
	public void testMoveWorldFocusWest() {
		portal.setWorldFocus(new Point2D.Float(50F,50F));
		portal.moveWorldFocusWest(1);
		assertEquals(49F, portal.worldFocus.x, 0);
		assertEquals(50F, portal.worldFocus.y, 0);
	}

	/**
	 * Test method for {@link cc.plural.tile.Portal#viewWorld(cc.plural.tile.PortalSnapshot)}.
	 */
	public void testViewWorld() {
		
	}

}
